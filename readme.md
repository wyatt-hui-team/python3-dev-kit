### Dependencies
* Python 3.x
* pip3

### Installation
Create an environment
* /

    ```
    python3 -m venv venv
    ```

Activate the environment

* /

    Before you work on your project, activate the corresponding environment
    ```
    . venv/bin/activate
    ```

    Upgrade pip and setuptools
    ```
    pip3 install --upgrade pip
    pip3 install --upgrade setuptools
    ```

Installation for Python packages
* /

    Copy `.env.example` to `.env`
    ```
    pip3 install -e .
    ```

### Deployment instructions
For version update, please edit the version number in
* setup.py

### Team contacts
* Somebody (somebody@example.com)

### License
Somebody
